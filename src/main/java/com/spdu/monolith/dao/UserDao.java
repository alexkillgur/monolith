package com.spdu.monolith.dao;

import com.spdu.monolith.model.User;

import java.util.List;

public interface UserDao {
    List<User> getUsersList();

    void add(User user);

    boolean remove(User user);
}
