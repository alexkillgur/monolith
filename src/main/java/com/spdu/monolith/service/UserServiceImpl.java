package com.spdu.monolith.service;

import com.spdu.monolith.dao.UserDao;
import com.spdu.monolith.model.User;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao dao;

    public UserServiceImpl(UserDao dao) {
        this.dao = dao;
    }

    public List<User> getUsersList() {
        return dao.getUsersList();
    }

    public void add(User user) {
        dao.add(user);
    }

    @Override
    public boolean remove(User user) {
        return dao.remove(user);
    }
}
